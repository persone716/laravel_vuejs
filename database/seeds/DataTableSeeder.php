<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class DataTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('data')->insert([
            'data' => self::getRandomJsonString(10),
            'access' => true,
        ]);

        DB::table('data')->insert([
            'data' => self::getRandomJsonString(10),
            'access' => false,
        ]);
    }

    /**
     * Генерация рандомной json строки
     *
     * @param $length
     * @return string
     */
    static private function getRandomJsonString($length){
        $some_array = [];
        for($i = 0; $i < $length; $i++){
            $some_array[] = [
                Str::random(10) => Str::random(20)
            ];
        }

        return json_encode($some_array);
    }
}
