<?php

use Illuminate\Http\Request;

// Get record by id
Route::post('get-record', 'DataController@getDataById');