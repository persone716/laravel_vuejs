<?php

namespace App\Policies;

use App\User;
use App\Model\DataModel;
use Illuminate\Auth\Access\HandlesAuthorization;

class DataPolicy
{
    use HandlesAuthorization;

    public function show(User $user, DataModel $dataModel){
        return $dataModel->access==1?true:false;
    }
}
