<?php

namespace App\Http\Controllers;

use App\Model\DataModel;
use App\Policies\DataPolicy;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class DataController extends Controller
{
    /**
     * Возврат записи по $id
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function getDataById(Request $request){
        $record = DataModel::find($request->id);
        if(empty($record)){
            return response(['message'=>'Записи с данным идентификатором не существует!'], 400);
        }

        $user = new User;
        if ($user->can('show', $record)) {
            return response($record, 200);
        } else {
            return response(['message'=>'Запись запрещена!'], 403);
        }
    }
}
