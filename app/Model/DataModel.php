<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DataModel extends Model
{
    public $table = 'data';
}
